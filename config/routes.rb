Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root :to => 'recommendations#methods'

  get 'test', to: 'recommendations#test'
  get 'recommendations', to: 'recommendations#methods'
  get 'recommendations/bestsellers', to: 'recommendations#bestsellers'
  get 'recommendations/mlt', to: 'recommendations#mlt'
  get 'recommendations/mltba', to: 'recommendations#mltba'
  get 'recommendations/mltbf', to: 'recommendations#mltbf'

  post 'generate_jwt', to: 'authorization#generate_token'
end
