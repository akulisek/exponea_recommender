class JsonWebToken
  class << self
    def encode(payload, expiration = 1.hours.from_now)
      payload[:exp] = expiration.to_i

      # this encodes the user data(payload) with our secret key
      return JWT.encode(payload, ENV['JWT_SECRET_KEY']), expiration.to_i
    end

    def decode(token)
      body = JWT.decode(token, ENV['JWT_SECRET_KEY'])[0]
      Hash.new(body)

      rescue JWT::ExpiredSignature, JWT::VerificationError => e
        raise ExceptionHandler::ExpiredSignatureError, e.message
      rescue JWT::DecodeError, JWT::VerificationError => e
        raise ExceptionHandler::DecodingError, e.message
      end
  end
end