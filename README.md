# README

# Exponea coding challenge - Recommender

Author: Adam Kulisek

The report can be found in this reposity as [AdamKulisek_Exponea_recommender_report.pdf](https://gitlab.com/akulisek/exponea_recommender/blob/master/AdamKulisek_Exponea_recommender_report.pdf)

## Source code

### Data preprocessing and indexing

The script I've created to index the catalog data into Elasticsearch is called [ElasticsearchDataLoader](https://gitlab.com/akulisek/exponea_recommender/blob/master/app/services/elasticsearch_data_loader.rb) and the script for loading the data into the DB is called [RecordCreator](https://gitlab.com/akulisek/exponea_recommender/blob/master/app/services/record_creator.rb)

### REST API 

The application controllers can be found in [controllers folder](https://gitlab.com/akulisek/exponea_recommender/tree/master/app/controllers)
The application models can be found in [models folder](https://gitlab.com/akulisek/exponea_recommender/tree/master/app/models)
The main business logic of the recommender can be found in [services fodler](https://gitlab.com/akulisek/exponea_recommender/tree/master/app/services) in a file called [recommendation_services.rb](https://gitlab.com/akulisek/exponea_recommender/blob/master/app/services/recommendation_services.rb)

### REST API endpoints

The REST API contains these endpoints
```
    GET /recommendations             : Returns all available recommendation methods with a brief description
    GET /recommendations/bestsellers : 
    GET /recommendations/mlt         : 
    GET /recommendations/mltba       :
    GET /recommendations/mltbf       : 
    
    POST /generate_jwt               : Expects a JSON body - {"secret":"exponea_recommender_secret"}. Returns a JWT token that expires after an hour.
```

Each recommendation method endpoint expects a required parameter: `?number_of_results=10`. 
Every recommendation method except for `/recommendationr/bestsellers` also expects another required parameter: `&user_id=yxz`
You can also pass an optional parameter `&recommend_at=timestamp_miliseconds`. This specifies a time when we want to generate the recommendations at.

## Elasticsearch configuration and usage

### MLT query example (content-based recommendations)
```
GET /_search
{
    "size": 10, 
    "query": {
      "bool": {
        "must": {
          "more_like_this" : {
              "fields" : ["description", "category_path", "gender","brand"],
              "like" : [
                {
                  "_id": "wMTLyWcBAET0IB2civ4x",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "mMTLyWcBAET0IB2cgvyn",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "EMTLyWcBAET0IB2cO-zP",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "-8TKyWcBAET0IB2czchJ",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "QsTKyWcBAET0IB2cssCV",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "ncXLyWcBAET0IB2cvg_V",
                  "_index": "products",
                  "_type": "product"
                }
              ],
              "min_term_freq" : 1,
              "max_query_terms" : 25,
              "boost": 3
          }
         },
         "should": {
           "more_like_this": {
             "fields" : ["description", "category_path", "gender","brand"],
              "like" : [
                {
                  "_id": "88TLyWcBAET0IB2cLuiZ",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "EcTLyWcBAET0IB2cO-zR",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "XMTKyWcBAET0IB2c1syA",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "_8TLyWcBAET0IB2cC97l",
                  "_index": "products",
                  "_type": "product"
                },
                {
                  "_id": "EMTLyWcBAET0IB2cO-zP",
                  "_index": "products",
                  "_type": "product"
                }
              ],
              "min_term_freq" : 1,
              "max_query_terms" : 25,
              "boost": 0
           }
         }
        }
       }
    }
}
```

### Index settings
```
PUT /products
{
"settings" : {
    "analysis" : {
        "filter" : {
            "english_stop" : {
                "type" :       "stop",
                "stopwords" :  "_english_"
            },
            "english_stemmer" : {
                "type" :      "stemmer",
                "language" :  "english"
            },
            "english_possessive_stemmer" : {
                "type" :       "stemmer",
                "language" :"possessive_english"
            },
            "my_char_filter":  {
                "type": "pattern_replace",
                "pattern": "(\\d+)",
                "replacement": ""
            }
        },
        "char_filter": {
            "quotes_mapping": {
                "type": "mapping",
                "mappings": [
                  "\\u0091=>\\u0027",
                  "\\u0092=>\\u0027",
                  "\\u2018=>\\u0027",
                  "\\u2019=>\\u0027",
                  "\\u201B=>\\u0027"
                ]
            }
        },
        "analyzer": {
            "products_english": {
                "tokenizer": "standard",
                "char_filter": [ "quotes_mapping" ],
                "filter": [
                  "my_char_filter",
                  "lowercase",
                  "english_possessive_stemmer",
                  "english_stemmer",
                  "english_stop"
                ]
            }
        }
    }
},
"mappings" : {
  "product" : {
     "properties" : {
        "product_id": {
                "type": "integer"
        },
        "category_id": {
                "type": "integer"
        },
        "category_path": {
                "type":"text",
                "analyzer":"products_english"
        },
        "brand": {
                "type": "text"
        },
        "gender": {
                "type": "text"
        },
        "description": {
              "type":"text",
              "analyzer":"products_english"
        },
        "price": {
                "type": "integer"
        }
      }
    }
  }
}
```
