class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.integer :product_id, index: true
      t.integer :category_id, index: true
      t.string :elasticsearch_id, index: true

      t.string :category_path
      t.string :brand, index: true
      t.string :gender, index: true
      t.float :price

      t.timestamps
    end
  end
end
