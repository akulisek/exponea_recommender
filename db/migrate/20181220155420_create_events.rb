class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.belongs_to :user, index: true
      t.belongs_to :product, index: true
      t.integer :event_type
      t.timestamp :created_at

      t.timestamps
    end
  end
end
