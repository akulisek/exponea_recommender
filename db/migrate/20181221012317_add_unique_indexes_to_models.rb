class AddUniqueIndexesToModels < ActiveRecord::Migration[5.1]
  def change
    remove_index :users, :customer_id
    remove_index :products, :product_id
    remove_index :products, :elasticsearch_id

    add_index :users, :customer_id, unique: true
    add_index :products, :product_id, unique: true
    add_index :products, :elasticsearch_id, unique: true
  end
end
