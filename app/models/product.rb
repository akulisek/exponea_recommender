class Product < ApplicationRecord
  has_many :events

  class << self
    # Returns N top sold Products' category_ids
    def n_top_sold(n, recommend_at)
      product_ids = Event.group(:product_id)
                        .where(event_type: :purchase_item)
                        .where('occurred_at < ?', recommend_at)
                        .order('count(*) desc')
                        .count.map{|pid,_| pid}.first(n)
      Product.where(id: product_ids).pluck(:product_id)
    end
  end
end
