class Event < ApplicationRecord
  belongs_to :user
  belongs_to :product

  scope :page_views, -> { where 'event_type = 0' }
  scope :additions_to_cart, -> { where 'event_type = 1' }
  scope :purchases, -> { where 'event_type = 2' }

  enum event_type: [:view_product, :add_to_cart, :purchase_item]
end
