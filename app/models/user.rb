class User < ApplicationRecord
  has_many :page_views, -> { where 'event_type = 0' }, class_name: 'Event'
  has_many :additions_to_cart, -> { where 'event_type = 1' }, class_name: 'Event'
  has_many :purchases, -> { where 'event_type = 2' }, class_name: 'Event'
end
