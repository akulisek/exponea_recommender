class RecommendationServices
  # Contains methods used to recommend Products to Users based on their implicit feedback Events.

  ELASTICSEARCH_URL = "#{ENV['ELASTICSEARCH_HOST']}:#{ENV['ELASTICSEARCH_PORT']}"

  # Initialize service
  #
  # Arguments:
  #     'n': the amount of products to recommend to a user
  def initialize(n, recommend_at)
    @n = n.to_i
    @recommend_at = DateTime&.strptime(recommend_at.to_s,'%s') || DateTime.now
    @top_n_sold_products = Product.n_top_sold(@n, @recommend_at)
  end

  # Recommends most popular Products
  #
  # Returns an array of N product_ids in a descending popularity order.
  def recommend_most_popular
    @top_n_sold_products.sort
  end

  # Recommends Products using Elasticsearch's MLT query. Pioritizes results that are similar to the products that a
  # User has bought. Products, that the User has viewed are less important. Though, if a User has not bought any
  # Products, the results will be similar only to the ones he has viewed previously.
  #
  # Returns an array of N product_ids in a descending fit (likeliness of user's satisfaction with the result) order.
  def recommend_mlt(customer_id)
    user = User.find_by_customer_id(customer_id)
    bought_events_with_products = Event.includes(:product).where('occurred_at < ?', Event.last.occurred_at)
                                      .where(user_id: user.id).where(event_type: :purchase_item)
    viewed_events_with_products = Event.includes(:product).where('occurred_at < ?', Event.last.occurred_at)
                                      .where(user_id: user.id).where(event_type: :view_product)

    bought_product_elasticsearch_ids = bought_events_with_products.map{|e| e.product.elasticsearch_id}
    viewed_product_elasticsearch_ids = viewed_events_with_products.map{|e| e.product.elasticsearch_id}

    results = mlt_query_elasticsearch(customer_id, bought_product_elasticsearch_ids, viewed_product_elasticsearch_ids,nil)
    results[0..@n-1].sort
  end

  # Recommends Products using Elasticsearch's MLT query and bestsellers. One third of the recommendations consist of
  # bestsellers, the rest is the results from Elasticsearch's MLT.
  #
  # Returns an array of N product_ids in a descending fit (likeliness of user's satisfaction with the result) order.
  def recommend_mlt_and_most_popular_fixed(customer_id)
    mlt_results = recommend_mlt(customer_id)

    results = @top_n_sold_products[0..(@n/3 - 1)]
    results += mlt_results[0..(@n - @n/3 - 1)]
  end

  # Recommends Products using Elasticsearch's MLT query. If there is insufficient user activity (none or too little to
  # produce enough recommendations), append most popular products to the recommendations.
  #
  # Returns an array of N product_ids in a descending fit (likeliness of user's satisfaction with the result) order.
  def recommend_mlt_and_most_popular_appended(customer_id)
    results = []

    mlt_results = recommend_mlt(customer_id)

    results += mlt_results[0..@n-1] unless mlt_results.empty?
    if results.size < @n
      results += @top_n_sold_products[0..@n-(results.size+1)]
    end
    results.sort
  end

  # Creates MLT query and sends it to Elasticsearch.
  #
  # Arguments:
  #   'user_id': who are we creating recommendations for.
  #   'products_ary': array of products the user has interacted with (purchased, added to cart, viewed).
  #   'timestamp': at what time do we want to generate recommendations for the user.
  #
  # Returns an array of recommended Products' ids.
  def mlt_query_elasticsearch(customer_id, bought_product_elasticsearch_ids, viewed_product_elasticsearch_ids, timestamp)
    json = {
        :size => @n,
        :query => {
            :bool => {
                :must => {
                    :more_like_this => {
                        :fields => ['description', 'category_path', 'gender', 'brand'],
                        :like => bought_product_elasticsearch_ids.empty? ? build_like_array_for_mlt(viewed_product_elasticsearch_ids) : build_like_array_for_mlt(bought_product_elasticsearch_ids),
                        :min_term_freq => 1,
                        :max_query_terms => 25
                    }
                }
            }
        }
    }

    if !bought_product_elasticsearch_ids.empty? && !viewed_product_elasticsearch_ids.empty?
      json[:query][:bool][:must][:more_like_this][:boost] = 3
      json[:query][:bool][:should] = {
          :more_like_this => {
              :fields => ['description', 'category_path', 'gender', 'brand'],
              :like => build_like_array_for_mlt(viewed_product_elasticsearch_ids),
              :min_term_freq => 1,
              :max_query_terms => 25,
              :boost => 0
          }
      }
    end

    puts json

    url = "#{ELASTICSEARCH_URL + '/products/product/_search'}"
    response = HTTParty.get(url,
                            :body => json.to_json,
                            :headers => { 'Content-Type' => 'application/json' })

    parsed_response = JSON.parse(response.body)

    parsed_response['hits']['hits'].inject([]) {|ary, hit| ary << hit['_source']['product_id']}
  end

  # Builds the 'like' part of the Elasticsearch's MLT query for specified products.
  #
  # Arguments:
  #   'product_elasticsearch_ids' argument is an array consisting of the Elasticsearch ids of given Products.
  #
  # Returns an array of hashes.
  def build_like_array_for_mlt(product_elasticsearch_ids)
    product_elasticsearch_ids.inject([]) {|ary, peid| ary << { '_id' => "#{peid}",
                                                               '_index' => 'products',
                                                               '_type' => 'product'} }
  end
end