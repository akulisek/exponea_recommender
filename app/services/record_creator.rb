class RecordCreator
  # Contains methods to insert records from the indexed Elasticsearch documents and events .csv into the DB

  ELASTICSEARCH_URL = "#{ENV['ELASTICSEARCH_HOST']}:#{ENV['ELASTICSEARCH_PORT']}"
  INDEX_URL = '/products'
  SCROLL_PARAM = '1m'

  class << self
    # Creates new Product records from indexed category documents in Elasticsearch
    def create_products_from_elasticsearch
      body = {:size => 1000,
              :query => {:match_all => {}}}
      scroll_id, hits = scroll_elasticsearch_index(ELASTICSEARCH_URL + INDEX_URL, body, SCROLL_PARAM, true)

      while !hits.empty? do
        hits.each do |hit|
          product = Product.new

          source = hit['_source']

          product.product_id = source['product_id'].to_i
          product.category_id = source['category_id'].to_i
          product.elasticsearch_id = hit['_id']
          product.category_path = source['category_path'].gsub('|',' ').squeeze
          product.brand = source['brand']
          product.gender = source['gender']
          product.price = source['price'].to_i

          product.save!
        end

        body = {:scroll => SCROLL_PARAM,
                :scroll_id => scroll_id}

        scroll_id, hits = scroll_elasticsearch_index(ELASTICSEARCH_URL, body, SCROLL_PARAM, false)
      end
    end

    # Creates new User and Event records from csv dataset
    def create_users_and_events_from_csv
      csv_path = Rails.root.join('app','data','dataset_events.csv')

      # Reads .csv file per row
      CSV.foreach(csv_path,
                  headers: :first_row,
                  encoding: 'utf-8',
                  :row_sep => :auto,
                  :col_sep => ',') do |row|
        user = User.find_or_create_by!(customer_id: row['customer_id'].to_i)
        product_id = Product.find_by_product_id(row['product_id']).id
        Event.where(user: user,
                    product_id: product_id,
                    event_type: row['type'],
                    occurred_at: DateTime.strptime(row['timestamp'],'%s')).first_or_create!
      end
    end

    private

    def scroll_elasticsearch_index(endpoint, body, scroll_param, initial)
      url = if initial
              endpoint + "/_search?scroll=#{scroll_param}"
            else
              endpoint + "/_search/scroll"
            end
      response = HTTParty.post(url,
                               :body => body.to_json,
                               :headers => { 'Content-Type' => 'application/json' })

      parsed_response_body = JSON.parse(response.body)

      scroll_id = parsed_response_body['_scroll_id']
      hits = parsed_response_body['hits']['hits']
      return scroll_id, hits
    end
  end
end