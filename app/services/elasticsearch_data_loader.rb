require 'HTTParty'
require 'csv'
require 'json'

# Catalog row abstraction
class Item <
  Struct.new(:product_id, :category_id, :category_path, :brand, :gender,
             :description, :price)
end

class ElasticsearchDataLoader
  # Parses raw data from CSV source and indexes them into Elasticsearch

  # Regex used to filter out unwanted text from the dataset
  HTML_REGEX = /(<[^>]*>)|(&lt;((?!&gt;).)*&gt;)|(<!--((?!-->).)*-->)|(&lt;|&gt;|<!--|-->)|[^\S ]+/

  def initialize(csv_path: Rails.root.join('app','data','dataset_catalog.csv'),
                 els_host: ENV['ELASTICSEARCH_HOST'],
                 els_port: ENV['ELASTICSEARCH_PORT'],
                 els_index_path: '/products/product')
    @elasticsearch_url = els_host + ":" + els_port
    @index_url = @elasticsearch_url + els_index_path
    @csv = CSV.read(csv_path,
                    headers: :first_row,
                    encoding: 'utf-8',
                    :row_sep => :auto,
                    :col_sep => ',')
  end

  # Parses .csv data and indexes them to Elasticsearch
  def index_data
    items = parse_data

    items.each do |item|
      begin
        response = index_item(item)
      rescue StandardError => error
        puts "\tError: #{error}"
      else
        puts "\t Success: #{response}"
      end
    end
  end

  private

  # Parses data from .csv's row into a Struct
  def parse_data
    items = []
    @csv.each do |row|
      item = Item.new

      item.product_id = row['product_id'].to_i
      item.category_id = row['category_id'].to_i
      item.category_path = row['category_path'].gsub('|',' ').squeeze
      item.brand = row['brand']
      item.gender = row['gender']
      # Filters out unwanted HTML tags
      item.description = row['description'].gsub(HTML_REGEX, '').squeeze
      item.price = row['price'].to_i

      items << item
    end
    items
  end

  # Indexes item into Elasticsearch
  def index_item(item)
    puts "Indexing item to #{@index_url}"
    puts "Item: #{item.to_h.to_json}"
    HTTParty.post(@index_url,
                  :body => item.to_h.to_json,
                  :headers => { 'Content-Type' => 'application/json' }
    )
  end

  # Creates Elasticsearch index (mostly for index setup documentation purposes)
  def put_index(index_name: '/products')
    puts "PUT index #{@elasticsearch_url}"
    HTTParty.put(@elasticsearch_url + index_name,
                :body => {
                  "=settings" => {
                      "analysis" => {
                          "filter" => {
                              "english_stop"=> {
                                  "type"=>       "stop",
                                  "stopwords"=>  "_english_"
                              },
                              "english_stemmer"=> {
                                  "type"=>       "stemmer",
                                  "language"=>   "english"
                              },
                              "english_possessive_stemmer"=> {
                                  "type"=>       "stemmer",
                                  "language"=>   "possessive_english"
                              },
                              "my_char_filter"=> {
                                  "type"=> "pattern_replace",
                                  "pattern"=> "(\\d+)",
                                  "replacement"=> ""
                              }
                          },
                          "char_filter"=> {
                              "quotes_mapping"=> {
                                  "type"=> "mapping",
                                  "mappings"=> [
                                    "\\u0091=>\\u0027",
                                    "\\u0092=>\\u0027",
                                    "\\u2018=>\\u0027",
                                    "\\u2019=>\\u0027",
                                    "\\u201B=>\\u0027"
                                  ]
                              }
                          },
                          "analyzer"=> {
                              "games_english"=> {
                                  "tokenizer"=>  "standard",
                                  "char_filter"=> [ "quotes_mapping" ],
                                  "filter"=> [
                                    "my_char_filter",
                                    "lowercase",
                                    "english_possessive_stemmer",
                                    "english_stemmer",
                                    "english_stop"
                                  ]
                              }
                          }
                      }
                  },
                  "mappings" => {
                    "product" => {
                       "properties" => {
                          "product_id"=> {
                                  "type"=> "integer"
                          },
                          "category_id"=> {
                                  "type"=> "integer"
                          },
                          "category_path"=> {
                                  "type"=> "string"
                          },
                          "brand"=> {
                                  "type"=> "string"
                          },
                          "gender"=> {
                                  "type"=> "string"
                          },
                          "description"=> {
                                "type"=> "string",
                                "analyzer"=> "english"
                          },
                          "price"=> {
                                  "type"=> "integer"
                          }
                        }
                      }
                    }
                  }.to_json,
                :headers => { 'Content-Type' => 'application/json' })
  end
end

ElasticsearchDataLoader.new().index_data()

