class RecommendationsController < RestApiController

  def test
    render json: { test: 'Test' }
  end

  def methods
    render json: { methods: { bestsellers: { description: 'Returns N most popular items' },
                              mlt: { description: 'Returns N items via Elasticsearch\'s MLT query' },
                              mltba: { description: 'Returns N items via Elasticsearch\'s MLT query appended with bestsellers' },
                              mltbf: { description: 'Returns N items. 2/3 of N items are acquired via Elasticsearch\'s MLT query and 1/3 of N are bestsellers' } } }
  end

  def bestsellers
    number_of_results = params[:number_of_results]
    recommend_at = params[:recommend_at]
    results = RecommendationServices.new(number_of_results, recommend_at).recommend_most_popular
    render json: { recommendations: results }
  end

  def mlt
    user_id = params[:user_id]
    number_of_results = params[:number_of_results]
    recommend_at = params[:recommend_at]
    results = RecommendationServices.new(number_of_results, recommend_at).recommend_mlt(user_id)
    render json: { recommendations: results }
  end

  def mltba
    user_id = params[:user_id]
    number_of_results = params[:number_of_results]
    recommend_at = params[:recommend_at]
    results = RecommendationServices.new(number_of_results, recommend_at).recommend_mlt_and_most_popular_appended(user_id)
    render json: { recommendations: results }
  end

  def mltbf
    user_id = params[:user_id]
    number_of_results = params[:number_of_results]
    recommend_at = params[:recommend_at]
    results = RecommendationServices.new(number_of_results, recommend_at).recommend_mlt_and_most_popular_fixed(user_id)
    render json: { recommendations: results }
  end
end