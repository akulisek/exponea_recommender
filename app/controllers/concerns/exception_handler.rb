module ExceptionHandler
  extend ActiveSupport::Concern

  # Define custom Errors
  class AuthenticationError < StandardError; end
  class MissingTokenError < StandardError; end
  class InvalidTokenError < StandardError; end
  class ExpiredSignatureError < StandardError; end
  class DecodingError < StandardError; end

  # Define custom Error handlers
  included do
    rescue_from ActiveRecord::RecordInvalid, with: :unprocessable_entity
    rescue_from ActiveRecord::RecordNotFound, with: :unprocessable_entity
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    rescue_from ExceptionHandler::MissingTokenError, with: :unprocessable_entity
    rescue_from ExceptionHandler::InvalidTokenError, with: :unauthorized_request
    rescue_from ExceptionHandler::ExpiredSignatureError, with: :unauthorized_request
    rescue_from ExceptionHandler::DecodingError, with: :unauthorized_request
  end

  private

  # Respond with error message and status code 422
  def unprocessable_entity(e)
    render json: { message: e.message }, status: :unprocessable_entity
  end

  # Respond with error message and status code 401
  def unauthorized_request(e)
    render json: { message: e.message }, status: :unauthorized
  end
end