class AuthorizeApiRequest
  prepend SimpleCommand

  def initialize(headers = {})
    @headers = headers
  end

  def call
    authorize_request
  end

  private

  def authorize_request
    authorized = JsonWebToken.decode(http_auth_header)
    authorized || errors.add(:token, 'Invalid token') && nil
  end

  def http_auth_header
    if @headers['Authorization'].present?
      @headers['Authorization'].split(' ').last
    else
      errors.add(:token, 'Missing token')
      nil
    end
  end
end