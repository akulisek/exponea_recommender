class AuthorizationController < RestApiController
  skip_before_action :authenticate_request, only: [:generate_token]

  def generate_token
    token, expiration = JsonWebToken.encode(user: :exponea_recommender_user) if params[:secret] == ENV['JWT_SECRET_KEY']

    if token
      render json: { access_token: token, expiration: expiration, message: 'Token successfully generated'}
    else
      render json: { error: 'Invalid secret' }, status: :unauthorized
    end
  end
end