class RestApiController < ActionController::API
  before_action :authenticate_request

  include ExceptionHandler

  private

  def authenticate_request
    authenticated = AuthorizeApiRequest.call(request.headers).result
    render json: { error: 'Not Authorized' }, status: 401 unless authenticated
  end
end